package main

import (
	"fmt"
	"os"
	"log"
	"time"
	"database/sql"
	"strings"
	"encoding/csv"
	"bufio"
	"io"
	"strconv"
)

type Weather struct {
	schedule []int
	apiKey   string
	db       *sql.DB
}

func NewWeather(time1 int, time2 int, time3 int, time4 int, apiKey string) *Weather {
	db, err = sql.Open("mysql", "root:fodevs@/mydb")
	if err != nil {
		panic(err.Error())
	}

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}
	return &Weather{
		schedule: []int{time1, time2, time3, time4},
		apiKey:   apiKey,
		db:       db,
	}
}

func (weather *Weather) GetWeather() {

	for location, id := range index {
		queryUrl:=BuildUrl(&location)
		temp, err := MakeQuery(queryUrl)
		if err != nil {
			fmt.Println(err)
		}
		t ,_ := strconv.Atoi(temp)
		stmt, err := db.Prepare("INSERT INTO weather(temp, location, date) VALUES(?, ?, NOW())")
		if err != nil {
			log.Fatalln(err)
		}

		_, err = stmt.Exec((t-32)*1000/18, id)
		if err != nil {
			fmt.Println(err)

		}
	}

}
func (weather *Weather) InSchedule(curTime int) bool {

	for _, time := range weather.schedule {
		if time == curTime {
			return true
		}
	}
	return false
}
func (weather *Weather) Get4TimesInDayWeather() {
	var now int
	now = time.Now().Hour()
	if !weather.InSchedule(now) {
		return
	}


	for location, id := range index {
		queryUrl:=BuildUrl(&location)
		temp, err := MakeQuery(queryUrl)
		if err != nil {
			fmt.Println(err)
		}
		t ,_ := strconv.Atoi(temp)
		stmt, err := db.Prepare("INSERT INTO 4timesindayweather(date, location, hour, temp) VALUES(NOW(), ?, ?,?) ON DUPLICATE KEY UPDATE temp=VALUES(temp)")
		if err != nil {
			fmt.Println(err)
		}
		_, err = stmt.Exec(id, now, (t-32)*1000/18)
		if err != nil {
			fmt.Println(err)

		}
	}

}
func (weather *Weather) ClearOld() {

	stmt, err := db.Prepare("DELETE FROM weather WHERE date < DATE_SUB(NOW(), INTERVAL 7 DAY )")
	if err != nil {
		fmt.Println(err)
	}

	res, err := stmt.Exec()
	if err != nil {
		fmt.Println(err)
	}
	affect, err := res.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("deleted %d rows from weather\n", affect)

}

func (weather *Weather) GetLocationsFromDB() {

	rows, err := db.Query("select id, cityname, countryname from location")
	if err != nil {
		fmt.Println(err)
	}
	var id int
	var city, country string
	for rows.Next() {
		err = rows.Scan(&id, &city, &country)
		if err != nil {
			fmt.Println(err)

		}
		index[location{city, country}] = id
	}
}
func (weather *Weather) SyncLocations() {

	f, err := os.Open("locations.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	reader := csv.NewReader(bufio.NewReader(f))
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			fmt.Println(err)
		}
		if line[1] == "" {
			line[1] = "RU"
		}
		line[0] = strings.ToLower(line[0])
		line[1] = strings.ToLower(line[1])
		if line[0] != "" && index[location{line[0], line[1] }] == 0 {

			stmt, err := db.Prepare("insert into location (cityname,countryname) values (?,?) ON DUPLICATE KEY UPDATE cityname=VALUES(cityname) countryname=VALUES(countryname)")
			if err != nil {
				fmt.Println(err)
			}

			_, err = stmt.Exec(line[0], line[1])
			if err != nil {
				fmt.Println(err)

			}
			rows, err := db.Query("select LAST_INSERT_ID()")
			if err != nil { /* error handling */ }
			var id int
			for rows.Next() {
				err = rows.Scan(&id)
				if err != nil {
					fmt.Println(err)

				}
				index[location{line[0], line[1]}] = id

			}

		}

	}

}