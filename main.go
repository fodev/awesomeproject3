package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"

)

type location struct {
	city    string
	country string
}

var index = make(map[location]int)
var db *sql.DB
var err error

func main() {
	pastHour := make(chan bool)
	weather := NewWeather(3, 6, 15, 18, "168e96188739d6eda33e7d40938fefaa")
	weather.GetLocationsFromDB()
	weather.SyncLocations()

	go func() {
		for {
			time.Sleep(59 * time.Minute)
			pastHour <- true
		}
	}()

	go func() {
		for {
			time.Sleep(1 * time.Hour)
			weather.Get4TimesInDayWeather()
		}
	}()

	go func() {
		for {
			time.Sleep(24 * time.Hour)
			weather.ClearOld()
		}
	}()

	for {
		<-pastHour
		weather.GetWeather()

	}

}