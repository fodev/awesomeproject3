package main

import (
	"net/http"
	"net/url"
	"io/ioutil"
	"fmt"
	"encoding/json"
)



func BuildUrl(loc *location) (urlParsed string) {
	Url, _ := url.Parse("https://query.yahooapis.com/v1/public/yql")
	parameters := url.Values{}
	parameters.Add("q", "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\""+loc.city+", "+loc.country+"\")")
	parameters.Add("format", "json")
	Url.RawQuery = parameters.Encode()
	urlParsed = Url.String()
	return
}

func JsonDeepDecode(keys []string, jsonResult map[string]interface{}) map[string]interface{} {

		for _, value := range keys {
			if _, has := jsonResult[value]; has {
				jsonResult = jsonResult[value].(map[string]interface{})
			} else {
				break
			}

		}
		return jsonResult

}

func MakeQuery(weatherUrl string) (string, error){
	resp, err := http.Get(weatherUrl)
	if err != nil {
		fmt.Println("Connected Error")
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Cannot Read Information")
		return "", err	}
	if resp.StatusCode != 200 {
		fmt.Printf("Request  http error: [HTTP %s] %s", resp.Status, string(body))
		return "", err	}


	result := make(map[string]interface{})
	if err := json.Unmarshal(body, &result); err != nil {
		fmt.Printf("Result JSON decode error (%#v): %s",  err, string(body))
		return "", err
	}
	keys:=[]string{"query","results","channel","item","condition"}
	result=JsonDeepDecode(keys,result)
	return  result["temp"].(string),nil


}